package com.webservice.webservice;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONObject;

import java.util.HashMap;


public class MainActivity extends Activity {


    private EditText etName,etMobile, etAddress;
    private Button btnSubmit;
    private ProgressDialog pDialog;
    private JSONObject json;
    private int success=0;
    private HTTPURLConnection service;
    private String strname ="", strMobile ="",strAddress="";
    private String path = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_main );

        etName=(EditText) findViewById( R.id.etName );
        etMobile= (EditText) findViewById(R.id.etMobile);
        etAddress= (EditText) findViewById(R.id.etAddress);
        btnSubmit= (Button) findViewById(R.id.btnSubmit);


        service = new HTTPURLConnection();
        btnSubmit.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!etName.getText().toString().equals("") && !etMobile.getText().toString().equals(""))
                {
                    strname = etName.getText().toString();
                    strMobile = etMobile.getText().toString();
                    strAddress = etAddress.getText().toString();

                    new PostDataToServer().execute();
                }
                else
                {
                    Toast.makeText( getApplicationContext(),"Please Enter all fields", Toast.LENGTH_LONG).show();
                }

            }
        } );
    }

    private class PostDataToServer extends AsyncTask<Void,Void,Void>
    {
        String response="";
        HashMap<String,String> postDataParms;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pDialog = new ProgressDialog(MainActivity.this);
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            return null;
        }
    }
}
